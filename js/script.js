// initiate art work data
data = [
    {"artist": "Amr Abo-Amer", "size": "70 x 100 cm", "media": "mixed media on canvas","img": "1.jpg"},
    {"artist": "Abd Al-Kareem Mahfouz","size": "100 x 100 cm","media": "oil on canvas","img": "2.jpg"},
    { "artist": "Shadi Al-Melhem","size": "70 x 100 cm", "media": "mixed media on canvas","img": "3.jpg" },
    {"artist": "Zaina Tatou","size": "100 x 100 cm","media": "Acrylic on canvas","img": "4.jpg"},
    {"artist": "Emel Aflak","title": "Fog 1","size": "70 x 100 cm","media": "mixed media on canvas","img": "5.jpg"},
    {
    "artist": "Sulioman Al-Quosi",
    "size": "70 x 70 cm",
    "media": "mixed media on canvas",
    "img": "6.jpg"
    },
    {
    "artist": "Housam Sirghaya",
    "title": "Transparant",
    "size": "90 x 80 cm",
    "media": "mixed media on canvas",
    "img": "7.jpg"
    },
    {
    "artist": "Pierre Hamati",
    "title": "Death's Color Is Khaki",
    "size": "95 x 75 cm",
    "media": "mixed media on canvas",
    "img": "8.jpg"
    },
    {
    "artist": "Sara Otabashi",
    "size": "70 x 70 cm",
    "media": "mixed media on canvas",
    "img": "9.jpg"
    },
    {
    "artist": "Kareem Al-Khayat",
    "size": "120 x 100 cm",
    "media": "mixed media on canvas",
    "img": "10.jpg"
    },
    {
    "artist": "Shahed Al-Rez",
    "size": "33 x 35 cm",
    "media": "Soft Pastil on paper",
    "img": "11.jpg"
    },
    {
    "artist": "Emel Aflak",
    "title": "Fog 01",
    "size": "16 x 11.5 cm",
    "media": "Ink on Coton paper",
    "img": "12.jpg"
    },
    {
    "artist": "Zaina Tatou",
    "size": "18x 26 cm",
    "media": "mixed media on paper",
    "img": "13.jpg"
    },
    {
    "artist": "Housam Sirghaya",
    "size": "19.5 x 9.5 cm",
    "media": "mixed media on amunition cardboard",
    "img": "14.jpg"
    },
    {
    "artist": "Leen Al-Kinani",
    "size": "20 x 29.5 cm",
    "media": "mixed media on paper",
    "img": "15.jpg"
    },
    {
    "artist": "Pierre Hamati",
    "title": "Restriction 2",
    "size": "40,5 x 12 cm",
    "media": "mixed media on paper",
    "img": "16.jpg"
    },
    {
    "artist": "Sara Otabashi",
    "size": "23 x 23 cm",
    "media": "mixed media on MDF",
    "img": "17.jpg"
    },
    {
    "artist": "Suliman Al-Qousi",
    "size": "22 x 17 cm",
    "media": "Ink on paper",
    "img": "18.jpg"
    },
    {
    "artist": "Samar Doud",
    "size": "28 x 30 cm",
    "media": "mixed media on paper",
    "img": "19.jpg"
    },
    {
    "artist": "Abd Al-Kareem Mahfouz",
    "size": "25 x 25 cm",
    "media": "oil on canvas",
    "img": "20.jpg"
    },
    {
    "artist": "Leen Al-Kinani",
    "title": "Can's Generation",
    "size": "100 x 70 cm",
    "media": "mixed media on canvas",
    "img": "21.jpg"
    },
    {
    "artist": "Shahed Al-Rez",
    "title": "The Flood",
    "size": "100 x 70 cm",
    "media": "mixed media on MDF",
    "img": "22.jpg"
    },
    {
    "artist": "Mohammad Ramadan",
    "size": "70 x 70 cm",
    "media": "mixed media on canvas",
    "img": "23.jpg"
    },
    {
    "artist": "Mayar Obedo",
    "title": "3 Apples",
    "size": "100 x 70 cm",
    "media": "mixed media on canvas",
    "img": "24.jpg"
    },
    {
    "artist": "Samar Daoud",
    "size": "70 x 70 cm",
    "media": "mixed media on canvas",
    "img": "25.jpg"
    },
    {
    "artist": "Mustafa Ali",
    "type": "councile",
    "size": "221 x 90 x 47 cm",
    "material": "Wood and Bronz",
    "img": '26.jpg'
    },
    {
    "artist": "Mustafa Ali",
    "type": "Dining tabel",
    "size": "357 x 80 x 77 cm",
    "material": "Wood and Bronz",
    }
];

// initiate panorama viewer
var pano = pannellum.viewer('panorama', {
    "default": {
        "firstScene": "area2",
        "sceneFadeDuration": 3000
    },
    "autoLoad": false,
    "avoidShowingBackground": true,
    "hfov": 93.5096167415145,
    "maxHfov": 93.5096167415145,
    "hotSpotDebug": false,
    "preview": '../imgs/prev.jpg',
    "scenes": {
        "area2": {	
            "type": "multires",
            "minPitch": -32,
            "maxPitch": 32,
            "multiRes": {
                "basePath": "./imgs/area2",
                "path": "/%l/%s%y_%x",
                "fallbackPath": "/fallback/%s",
                "extension": "jpg",
                "tileResolution": 512,
                "maxLevel": 5,
                "cubeResolution": 4752
            },
            "hotSpots": [
                {
                    "type": "scene",
                    "pitch": -18,
                    "yaw": -0,
                    "text": "Angle 2",
                    "sceneId": "area1",
                },
                {
                    "type": 'info',
                    'pitch': 6.9882294474405775, 
                    'yaw': -7.290772454803857, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[0],
                },
                {
                    "type": 'info',
                    'pitch': 6.718239349063129, 
                    'yaw': 2.1714278973896057, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[1]
    
                },
                {
                    "type": 'info',
                    'pitch': 6.693280904386974, 
                    'yaw': 9.172277194695463, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[2]
                },
                
                {
                    "type": 'info',
                    'pitch':  6.323053828053036,
                    'yaw': 20.13469891725962, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[3]
                },
                {
                    "type": 'info',
                    'pitch': 5.681954681989598, 
                    'yaw': 25.05710795493578, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[4]
                },
                {
                    "type": 'info',
                    'pitch': 4.5975035340564885, 
                    'yaw': 30.944386565958702, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[5]
                },
                {
                    "type": 'info',
                    'pitch': 1.290461883964491, 
                    'yaw': 42.01104751669764, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[6]
                },
                {
                    "type": 'info',
                    'pitch': 0.016980823458719676, 
                    'yaw': 62.86321083214561, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[7]
                },
                {
                    "type": 'info',
                    'pitch': -0.5132779323112496, 
                    'yaw': 89.87100203031817, 
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[8]
                },
                {
                    "type": 'info',
                    'pitch':  -3.1841750760255536, 
                    'yaw':  128.66974505718056,
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[9]
                },
                
                {
                    "type": 'info',
                    'pitch':  8.557638316811957,
                    'yaw': 168.16978693754555,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[10]
                },
                {
                    "type": 'info',
                    'pitch': 11.470440909102509,
                    'yaw': 177.7098574082796,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[11]
                },
                {
                    "type": 'info',
                    'pitch': 9.870867232074632,
                    'yaw': -168.96866662746353,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[12]
                },
                {
                    "type": 'info',
                    'pitch': 12.285474631408174,
                    'yaw': -159.0676739972581,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[13]
                },
                {
                    "type": 'info',
                    'pitch': 10.472767057750312,
                    'yaw': -148.2836777792776,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[14]
                },
                
                {
                    "type": 'info',
                    'pitch': 0.9136485308372179,
                    'yaw': 169.1473018504066,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[15]
                },
                {
                    "type": 'info',
                    'pitch': 2.918740943139747,
                    'yaw': 177.88682422036578,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[16]
                },
                {
                    "type": 'info',
                    'pitch': -0.09453635868551565,
                    'yaw': -169.72594536709556,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[17]
                },
                {
                    "type": 'info',
                    'pitch': 0.2711123618174817,
                    'yaw': -157.20968375641044,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[18]
                },
                {
                    "type": 'info',
                    'pitch': 4.375827011720908,
                    'yaw':  -147.6966682832711,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[19]
                },
                
                {
                    "type": 'info',
                    'pitch': 2.083518607405242,
                    'yaw':  -108.29955834878865,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[20]
                },
                
                {
                    "type": 'info',
                    'pitch': 5.299962840443265,
                    'yaw':  -31.321514509159645,
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[21]
                },
                {
                    "type": 'info',
                    'pitch':  6.355793951413929,
                    'yaw':  -25.163733773653156,
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[22]
                },
                {
                    "type": 'info',
                    'pitch':  6.821759914498986,
                    'yaw':  -19.971104704581702,
                    'hfov':  22.369528619528623,
                    "clickHandlerArgs": data[23]
                },
                {
                    "type": 'info',
                    'pitch': 7.322534347362798,
                    'yaw':  -17.334099110869367,
                    'hfov': 22.369528619528623,
                    "clickHandlerArgs": data[24]
                },

                {
                    "type": 'info',
                    'pitch': -14.906682651352034,
                    'yaw':  13.517803278199434,
                    'hfov': 93.5096167415145,
                    "clickHandlerArgs": data[26]
                },
                {
                    "type": 'info',
                    'pitch': -7.516065500377376,
                    'yaw':  -147.98745293057488,
                    'hfov': 70.37792109741817,
                    "clickHandlerArgs": data[25]
                },

            ]
        },
        "area1": {	
            "type": "multires",
            "minPitch": -32,
            "maxPitch": 32,
            "pitch": -3.5979572867706855,
            "yaw": -7.57024525418899,
            "multiRes": {
                "basePath": "./imgs/area1",
                "path": "/%l/%s%y_%x",
                "fallbackPath": "/fallback/%s",
                "extension": "jpg",
                "tileResolution": 512,
                "maxLevel": 5,
                "cubeResolution": 7016
            },
            "hotSpots": [
                {
                    "type": "scene",
                    "pitch": -26,
                    "yaw": -4,
                    "text": "Angle 1",
                    "sceneId": "area2"
                },

                {
                    "type": "info",
                    "pitch": -0.5359325850398142,
                    "yaw": 146.21205798820907,
                    "hfov": 70.37792109741817,
                    "clickHandlerArgs": data[0]

                },
                {
                    "type": "info",
                    "pitch": -2.182773071741861,
                    "yaw": -179.97221874905017,
                    "hfov": 70.37792109741817,
                    "clickHandlerArgs": data[1]

                },
                {
                    "type": "info",
                    "pitch": -0.9656719981369736,
                    "yaw": -157.35242042603045,
                    "hfov": 70.37792109741817,
                    "clickHandlerArgs": data[2]

                },

                {
                    "type": "info",
                    "pitch": -1.7633304215380725,
                    "yaw": -96.19066714395564,
                    "hfov": 70.37792109741817,
                    "clickHandlerArgs": data[3]

                },
                {
                    "type": "info",
                    "pitch": -0.8237098929048114,
                    "yaw": -68.65301787731806,
                    "hfov": 70.37792109741817,
                    "clickHandlerArgs": data[4]

                },
                {
                    "type": "info",
                    "pitch": -0.2434611544951915,
                    "yaw": -52.80555737447031,
                    "hfov": 70.37792109741817,
                    "clickHandlerArgs": data[5]

                },
                {
                    "type": "info",
                    "pitch": -0.421363740028059,
                    "yaw": -40.77584318168908,
                    "hfov": 30.56757741356688,
                    "clickHandlerArgs": data[6]

                },
                {
                    "type": "info",
                    "pitch": 1.6726415613293957,
                    "yaw": -33.14932847963553,
                    "hfov": 30.56757741356688,
                    "clickHandlerArgs": data[7]

                },
                {
                    "type": "info",
                    "pitch": 2.7635046739316937,
                    "yaw": -28.931299125111025,
                    "hfov": 30.56757741356688,
                    "clickHandlerArgs": data[8]

                },
                {
                    "type": "info",
                    "pitch": 1.5841849008572784,
                    "yaw": -24.491172048956173,
                    "hfov": 30.56757741356688,
                    "clickHandlerArgs": data[9]

                },
                
                {
                    "type": "info",
                    "pitch": 5.967729754445332,
                    "yaw": -11.86102559165013,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[10]

                },
                {
                    "type": "info",
                    "pitch": 6.818064189081895,
                    "yaw": -9.171367608764434,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[11]

                },
                {
                    "type": "info",
                    "pitch": 6.3525923778215745,
                    "yaw": -4.9162249500069155,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[12]

                },
                {
                    "type": "info",
                    "pitch": 7.164894528241658,
                    "yaw":  -1.871640255800524,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[13]

                },
                {
                    "type": "info",
                    "pitch": 6.516197592138303,
                    "yaw": 2.2939786109182814,
                    "hfov":  17.528465894485024,
                    "clickHandlerArgs": data[14]

                },

                {
                    "type": "info",
                    "pitch": 3.7192853967943336,
                    "yaw": -11.721435071990738,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[15]

                },
                {
                    "type": "info",
                    "pitch": 4.2301378983670945,
                    "yaw": -9.161028584143777,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[16]

                },
                {
                    "type": "info",
                    "pitch": 3.365397536696364,
                    "yaw": -5.355991415119527,
                    "hfov":  17.528465894485024,
                    "clickHandlerArgs": data[17]

                },
                {
                    "type": "info",
                    "pitch": 3.3136056093312427,
                    "yaw": -1.2691431245884628,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[18]

                },
                {
                    "type": "info",
                    "pitch": 4.439433824113833,
                    "yaw": 2.5188954227929363,
                    "hfov": 17.528465894485024 ,
                    "clickHandlerArgs": data[19]

                },

                {
                    "type": "info",
                    "pitch": 3.439910532504098,
                    "yaw":  9.99879654346234,
                    "hfov": 17.528465894485024,
                    "clickHandlerArgs": data[20]

                },
                
                {
                    "type": "info",
                    "pitch": 0.02405475300095214,
                    "yaw": 35.699846106902626,
                    "hfov": 66.16788526122376,
                    "clickHandlerArgs": data[21]

                },
                {
                    "type": "info",
                    "pitch": -0.3233311731644414,
                    "yaw": 53.097912224729264,
                    "hfov": 66.16788526122376,
                    "clickHandlerArgs": data[22]

                },
                {
                    "type": "info",
                    "pitch":  -0.9012007437551303,
                    "yaw": 86.50837314779933,
                    "hfov": 66.16788526122376,
                    "clickHandlerArgs": data[23]

                },
                {
                    "type": "info",
                    "pitch": 0.7183779109499435,
                    "yaw": 110.27887598279432,
                    "hfov":  66.16788526122376,
                    "clickHandlerArgs": data[24]

                },


                {
                    "type": "info",
                    'pitch': -20.70894558384406,
                    'yaw':  10.058258947398823,
                    'hfov': 76.09959166837673,
                    "clickHandlerArgs": data[26]

                },
                {
                    "type": "info",
                    "pitch": 0.6064746666560986,
                    "yaw": -0.6392844148332761,
                    "hfov":  -7.57024525418899,
                    "clickHandlerArgs": data[25]

                },
            ]
        }
    }
});

// replace main cover writings
$('.pnlm-load-button').html(`<img style="margin-bottom:10px; width: 50%;" src='./imgs/exhibition_logo.svg'>
<p class=''>
    One Color" brings together a group of the finest young Artists who chose black and white as a general language for their visual dialog.
    in this presentation and on the different concepts and methods, "One Color" puts the special and unique identity of each of them in front of the viewer, with all abstractness
    It collects in one scene a set of multiple anesthetic concepts of all syrian in a visual rhythm which reflects the reality	
</p>
<p style="">Between Black and White، Evrythig residents</p>
<p class=''>يجمع المعرض مجموعة من نخبة الشباب الذين انتقو الابيض و الاسود لغة عامة لحوارهم البصري في هذا العرض على اختلاف مفاهيم و اساليبهم مما يضع الهوية الخاصة و المتفردة لكل منهم أمام المشاهد بكل تجرد و يجمع في مشهد واحد مجموعة من المفاهيم الجمالية المتعددة للشباب السوري المقيم بايقاع بصري يعكس الواقع</p>
<p style='margin-bottom 20px;'>ما بين الابيض و الاسود يكمن كل شيء</p>
<p >From: 12/12/2020 Until 22/12/2020</p>
<p >Location: Gallery Mustafa Ali Old Damascus-Alameen street N 86 Damas Syria Damas, Syria</p>
<p class="click_open" style="margin-bottom: 100px;">Enter Virtual Tour</p>`);

pano.on('load', () => {
    // initiate hotspots click function
    $('.pnlm-info').on('click', (e, args) => {
        if (e.target.dataset.img) {
            if (e.target.dataset.artist == "Emel Aflak" && e.target.dataset.img == '5.jpg') {
                $('#eaart').show()
            } else if ($('#eaart')){
                $('#eaart').hide();
            }
            d = new Date();
            $('#artwork').attr('src', './imgs/ed_small/'+e.target.dataset.img+'?'+d.getTime());
        }
        if (e.target.dataset.artist) {
            $('#artist').text(e.target.dataset.artist)
        }
        if (e.target.dataset.title) {
            $('#title').text(e.target.dataset.title)
        } else {
            $('#title').html('Untitled')
        }
        if (e.target.dataset.size) {
            $('#size').text(e.target.dataset.size)
        }
        if (e.target.dataset.media) {
            $('#mediaP').show();
            $('#materialP').hide();
            $('#media').text(e.target.dataset.media)
        }
        if (e.target.dataset.material) {
            $('#mediaP').hide();
            $('#materialP').show();
            $('#material').text(e.target.dataset.material)
        }
        if (e.target.dataset.material) {
            $('#typeP').show();
            $('#type').text(e.target.dataset.type)
        } else {
            $('#typeP').hide();
        }
        
        
        $('.modal').modal('show');
    })

})


$('.modal').on('hidden.bs.modal', () => {
    $('#artwork').attr('src', '')
})

